/*
 * Lanzará la ventana principal de nuestra aplicación
 */
package ejemplografico;

/**
 *
 * @author mfontana
 */
public class EjemploGrafico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos nuestro JFrame principal
        Principal principal = new Principal();
        // Hacemos que se muestre centrado en la pantalla
        principal.setLocationRelativeTo(null);
        // Lo mostramos (está oculto hasta que no ejecutamos esta
        // instrucción)
        principal.setVisible(true);
    }
    
}
